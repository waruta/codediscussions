// iterator.c
#include <iostream> 
#include <vector> 
#include <list>
using namespace std; 

int main() 
{ 
	// Declaring a vector 
	vector<int> v = {1, 2, 3, 4, 5, 6}; 

	// Declaring an iterator 
	vector<int>::iterator i; 

	int j; 

	cout << "Without iterators = "; 

	// Accessing the elements without using iterators 
	for (j = 0; j < 3; ++j)  
	{ 
		cout << v[j] << " "; 
	} 

	cout << " " << endl ;
	cout << "With iterators = "; 

	// Accessing the elements using iterators 
	for (i = v.begin(); i != v.end(); ++i) 
	{ 
		cout << *i << " "; 
	} 

	// Using iterators to copy from vector to list

	list<int> l(v.begin(), v.end());

	// Using iterators to insert
	list<int>::iterator it = l.begin(); 
	l.insert(it++,3,9); // Be careful with pre- or post-increment

	cout << " " << endl;
	for (it = l.begin(); it != l.end(); ++it) 
	{ 
		cout << *it << " "; 
	} 
	
	// For vector iterators, pointer operations work
	while (i<v.end()){
		i=i+2;
	}
	
	// But not for list iterators ...
	while (it<l.end()){
		it=it+2;
	}

	return 0; 
} 
