# CodeDiscussions

This repository contains supplementary material for the weekly discussions on code development in the hadron physics division at Uppsala University.

## Format

Each presentation, including discussion, should take no more than 15 to 20 minutes. The following points should be considered for the preparation.

- What is it?
- Pros and Cons
- General example
- Specific example, relating to our work (if possible)
- Suggestion of a new topic

## Upcoming topics

- C++ iterators, e.g. std::vector (Jenny)

## Unassigned topics

- von Neumann bottleneck
- expressive diagnositics
- memory debugging (valgrind)
- sanitizers, valgrind, profilers
- debugger (gdb)
- code analysis tools
- std::unique_ptr vs std::shared_ptr vs std::weak_ptr

## Past topics

- constexpr (Jenny)
- gcc vs clang, general introduction (Adeel)
- pointers / smart pointers (Walter)