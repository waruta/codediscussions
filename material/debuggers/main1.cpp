
// g++ main.cpp -g -Wall -o main

#include<iostream>
using namespace std;
 
long factorial(int n);
int main()
{
    int n(0);
    cout << "Enter a number: ";
    cin>>n;
    long val=factorial(n);
    cout << "Factorial is: " << val << endl;
    cin.get();
    return 0;
}
 
long factorial(int n)
{
    long result(1);
    while(n--)				// fix: n > 0
    {
        result*=n;
        
        // fix: n--;
    }
    return result;
}

